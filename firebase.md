# FIREBASE

Firebase est un ensemble d'outils pour l'hébergement et le développement d'applications mobiles et web

## Instalation

```
npm install -g firebase-tools
firebase --help
```

### Créer un projet firebase

- Se rendre sur le site de [Firebase](https://console.firebase.google.com/u/0/) et cliquer sur **ajouter un projet**
- Suivre les étapes de création
- Dans le menu à gauche selectionner **Créer** puis **Firestore Database**, cliquer sur *Créer une base de données*
- Suivre les étapes
- Dans le menu à gauche selectionner ⚙️ puis **Paramètres du projet** puis compléter les infos de *Emplacement des ressources GCP par défaut*

## Implémentation dans le projet

- Créer un dossier **server** à la racine du projet, et déplacer vous dedans
- Lancer la commande `firebase init` et choisisr les options suivantes :
  - Firestore : Configure ...
  - Functions : Configure ...
  - Storage : Configure ...
  - Emulators : Set up ...
- Suivre les étapes qui suivent ( valider les choix par défaut )
- Puis pour *Emulators Setup* choisir les options nécessaires
  - Par exemple :
  - Authentication Emulator
  - Functions Emulator
  - Firestore Emulator
  - Storage Emulator
- Et suivre les étapes qui suivent ( valider les choix par défaut )

## Lancer les Emulateurs

- Toujours dans le dossier **server**, tapper la commande `firebase emulators:start`
- Aller voir à cette [adresse](http://127.0.0.1:4000/) (par defaut) pour voir les émulateurs

## Installer les API de firebase

- Sur VsCode : **ctrl + shift + p** > `Dart: Add Dependency` :
> firebase_core, firebase_auth, firebase_storage, cloud_firestore,cloud_functions
- En ligne de commande : 

> flutter pub add firebase_core firebase_auth firebase_storage cloud_firestore cloud_functions

### Ajouter des lignes de code dans main.dart
Dans `Future<void> main() async {` , ajouter : 

``` 
  // pour les emulateurs
  if (kDebugMode) {
    final String host = Platform.isAndroid ? "10.0.2.2" : "localhost";
    await FirebaseAuth.instance.useAuthEmulator(host, 9099);
    FirebaseFunctions.instance.useFunctionsEmulator(host, 5001);
    FirebaseFirestore.instance.useFirestoreEmulator(host, 8080);
    FirebaseStorage.instance.useStorageEmulator(host, 9199);
  }
  // pour éviter les infos en cache
  await FirebaseFirestore.instance.terminate();
  await FirebaseFirestore.instance.clearPersistence();
```

## POD INSTALL (pour ios)

## Flutter Fire

- Installation en ligne de commande : `dart pub global activate flutterfire_cli`
- Puis `flutterfire configure` et suivre les étapes
- Ajouter ces lignes de codes dans main.dart :

```
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
```
- Et celle ci dans `Future<void> main() async {` :
```
await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
```

## Flutter Bloc

Installation :

- Sur VsCode : **ctrl + shift + p** > `Dart: Add Dependency` :
> flutter_bloc, equatable
- En ligne de commande : 

> flutter pub add flutter_bloc, equatable
