# Theme Flutter

## Utilisation de Appainter

[Appainter](https://appainter.dev/#/) est un site avec des thèmes prédéfinis, que l'on peut modifier.

Le thème sera récupérable sous la forme d'un fichier json (export)

## Ajout à l'application 

### Dossier assets
- Créer un dossier ```assets```, à la racine du projet
- Ajouter le fichier json téléchargé, et le renommer (si besoin)

### Fichier Pubspec.yaml
```
flutter pub add json_theme
```

Ajouter ceci dans le fichier pubspec.yaml ( sous ```flutter :``` )
```
  assets:
    - assets/theme.json
```

### Fichier Main.dart

```
import 'package:json_theme/json_theme.dart';
import 'package:flutter/services.dart'; // for rootBundle
import 'dart:convert'; // for jsonDecode
```

```
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // pour le thème
  final themeStr = await rootBundle.loadString('assets/theme.json');
  final themeJson = jsonDecode(themeStr);
  final theme = ThemeDecoder.decodeThemeData(themeJson)!;

  runApp(MyApp(theme: theme));
}
```

```
return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      home: const Home(),
    );
```

### Utilisation
```
Text(
  'Bonjour Monde !',
  style: Theme.of(context).textTheme.displayMedium,
),
```